<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <style>
        .st {
            color: white !important;
        }
    </style>
  </head>
  <body>
    <header>
        <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img src="http://localhost/kamonitor/assets/image/logo.png" width="170" style="-webkit-filter: drop-shadow(0px 0px 50px rgba(255,255,255,1));">
        </a>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            <!--<li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>-->
            </ul>
            <span class="Timer navbar-text st">
            </span>
        </div>
        </nav>
    </header>
    <main>
        <table class="table">
            <thead>
                <tr>
                    <th>NOMOR KA</th>
                    <th>KERETA API</th>
                    <th>TUJUAN</th>
                    <th>BERANGKAT</th>
                    <th>JALUR</th>
                    <th>STATUS</th>
                    <th>KETERANGAN</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>19</td>
                    <td>ARGO PARAHYANGAN</td>
                    <td>GAMBIR</td>
                    <td>05.00</td>
                    <td>4</td>
                    <td>TELAH TERSEDIA</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>ARGO PARAHYANGAN</td>
                    <td>GAMBIR</td>
                    <td>05.00</td>
                    <td>4</td>
                    <td>TELAH TERSEDIA</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>ARGO PARAHYANGAN</td>
                    <td>GAMBIR</td>
                    <td>05.00</td>
                    <td>4</td>
                    <td>TELAH TERSEDIA</td>
                    <td>-</td>
                </tr>
            </tbody>
        </table>
    </main>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script>
    var start = new Date;

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function nameDay(i) {
        switch(i) {
            case 0:
                return "Minggu"
            case 1:
                return "Senin"
            case 2:
                return "Selasa"
            case 3:
                return "Rabu"
            case 4:
                return "Kamis"
            case 5:
                return "Jum\'at"
            case 6:
                return "Sabtu"
        }
    }

    function nameMonth(i) {
        switch(i) {
            case 0:
                return "Januari"
            case 1:
                return "Februari"
            case 2:
                return "Maret"
            case 3:
                return "April"
            case 4:
                return "Mei"
            case 5:
                return "Juni"
            case 6:
                return "Juli"
            case 7:
                return "Agustus"
            case 8:
                return "September"
            case 9:
                return "Oktober"
            case 10:
                return "November"
            case 11:
                return "Desember"
        }
    }

    setInterval(function() {
        start = new Date;
        $('.Timer').html(addZero(start.getHours())+
        ":"+addZero(start.getMinutes())+
        ":"+addZero(start.getSeconds())+
        "<br/>"+
        nameDay(start.getDay())+" , "+
        start.getDate()+" "+
        nameMonth(start.getMonth())+" "+
        start.getFullYear());
    }, 1000);
    </script>
</body>
</html>